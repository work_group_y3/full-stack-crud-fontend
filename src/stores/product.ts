import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import axios from "axios";
import product from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useProductStore = defineStore("Product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const products = ref<Product[]>([]);
  const editedProduct = ref<Product>({ name: "", price: 0 });

  watch(dialog, (newDialog, oldDiglog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedProduct.value = { name: "", price: 0 };
    }
  });

  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        // const res = await axios.post(
        //   "http://localhost:3000/products",
        //   editedProduct.value
        // );

        const res = await productService.saveProduct(editedProduct.value); // ติด Error
      }
      dialog.value = false;
      // clearProduct()
      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  async function deleteProduct(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await productService.deleteProduct(id);

      await getProducts();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  // function clearProduct() {
  //   editedProduct.value = { name: "", price: 0 };
  // }

  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }

  return {
    products,
    getProducts,
    dialog,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
  };
});
