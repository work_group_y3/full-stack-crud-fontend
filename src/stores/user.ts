import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import { isTemplateNode } from "@vue/compiler-core";
import userService from "@/services/user";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useUserStore = defineStore("User", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const isTable = ref(true);
  const users = ref<User[]>([]);

  const editedUser = ref<User>({
    login: "",
    name: "",
    password: "",
  });

  watch(dialog, (newDialog, oldDiglog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedUser.value = { login: "", name: "", password: "" };
    }
  });

  async function getUsers() {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUsers();
      users.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล User ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveUser() {
    loadingStore.isLoading = true;
    try {
      if (editedUser.value.id) {
        const res = await userService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
      } else {
        const res = await userService.saveUser(editedUser.value);
      }
      dialog.value = false;
      await getUsers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล User ได้");
    }
    loadingStore.isLoading = false;

    // if (editedUser.value.id < 0) {
    //   editedUser.value.id = lastId++;
    //   users.value.push(editedUser.value);
    // } else {
    //   const index = users.value.findIndex(
    //     (item) => item.id === editedUser.value.id
    //   );
    //   users.value[index] = editedUser.value;
    // }
    // dialog.value = false;
    // clear();
  }

  async function deleteUser(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await userService.deleteUser(id);

      await getUsers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล User ได้");
    }
    loadingStore.isLoading = false;
  }

  const editUser = (user: User) => {
    editedUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;

    // editedUser.value = { ...user };
    // dialog.value = true;
  };

  // const clear = () => {
  //   editedUser.value = {
  //     id: -1,
  //     login: "",
  //     name: "",
  //     password: "",
  //   };
  // };

  // let lastId = 4;
  // const users = ref<User[]>([
  //   { id: 1, login: "admin", name: "Administrator", password: "Pass@1234" },
  //   { id: 2, login: "user1", name: "User 1", password: "Pass@1234" },
  //   { id: 3, login: "user2", name: "User 2", password: "Pass@1234" },
  // ]);

  // const login = (loginName: string, password: string): boolean => {
  //   const index = users.value.findIndex((item) => item.login === loginName);
  //   if (index >= 0) {
  //     const user = users.value[index];
  //     if (user.password === password) {
  //       return true;
  //     }
  //     return false;
  //   }
  //   return false;
  // };
  // const deleteUser = (id: number): void => {
  //   const index = users.value.findIndex((item) => item.id === id);
  //   users.value.splice(index, 1);
  // };

  return {
    users,
    getUsers,
    dialog,
    editedUser,
    saveUser,
    deleteUser,
    editUser,
    isTable,
  };
});
